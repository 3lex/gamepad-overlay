import './sass/styles.sass'

var watcherLoop
var gamepad_now = null
var gamepad_last = null

var svg

const socket = io()
      socket.on('gamepad', onSocketGamepad)

export function onGamepadConnected(e) {
  socket.emit('gamepad-connected', true)
  document.getElementById('svg').classList.toggle('hidden', false)
}

export function onGamepadDisconnected(e) {
  socket.emit('gamepad-connected', false)
  document.getElementById('svg').classList.toggle('hidden', true)
  if (watcherLoop)
  {
    cancelAnimationFrame(watcherLoop)
  }
}

function gamepadLoop() {
  if (navigator.getGamepads().length)
  {
    gamepadPoll()
  }

  watcherLoop = requestAnimationFrame(gamepadLoop)
}

function gamepadPoll() {
  let gamepadNeedsUpdate = false

  gamepad_now = navigator.getGamepads()[0]

  if (gamepad_now && gamepad_last)
  {
    gamepad_now.axes.map((axis, i) => {
      if (axis !== gamepad_last.axes[i])
      {
        gamepadNeedsUpdate = true
      }
    })
    gamepad_now.buttons.map((btn, i) => {
      if (btn.value !== gamepad_last.buttons[i].value)
      {
        gamepadNeedsUpdate = true
      }
    })
  }

  if (gamepadNeedsUpdate)
  {
    socket.emit('gamepad-update', {
      id: gamepad_now.id,
      axes: gamepad_now.axes,
      buttons: gamepad_now.buttons.map((b,i) => { return {
        pressed: b.pressed,
        value: b.value
      }})
    })
    gamepadNeedsUpdate = false
  }
  gamepad_last = gamepad_now
}

function onSocketGamepad(gp) {
  document
    .getElementById('svg')
    .classList.toggle('hidden', !gp.id)

  gp.buttons.map((btn,i) => {
    let el = document.getElementById(`button-${i}`)
        el.classList.toggle('pressed', btn.pressed)
    if (i === 6 || i === 7)
    {
      if (btn.value > 0)
      {
        el.setAttributeNS(null, 'r', (1 - btn.value) * 5)
        el.classList.toggle('pressed', true)
      } else {
        el.setAttributeNS(null, 'r', 5)
      }
    }
  })

  let xLft = document.getElementById('button-17')
  if (xLft)
  {
    xLft.setAttributeNS(null, 'cx', Math.max(0, gp.axes[0]*5 + GamepadButtons[17].params.cx) || GamepadButtons[17].params.cx)
    xLft.setAttributeNS(null, 'cy', Math.max(0, gp.axes[1]*5 + GamepadButtons[17].params.cy) || GamepadButtons[17].params.cy)
  }
  let xRgt = document.getElementById('button-18')
  if (xRgt)
  {
    xRgt.setAttributeNS(null, 'cx', Math.max(0, gp.axes[2]*5 + GamepadButtons[18].params.cx) || GamepadButtons[18].params.cx)
    xRgt.setAttributeNS(null, 'cy', Math.max(0, gp.axes[3]*5 + GamepadButtons[18].params.cy) || GamepadButtons[18].params.cy)
  }
}

function createGamepadDisplay() {
  let svgNS = 'http://www.w3.org/2000/svg'
  let svg = document.createElementNS(svgNS, 'svg')
      svg.setAttributeNS(null, 'viewBox', '0 0 100 100')

  GamepadButtons.map(button => {
    let el = document.createElementNS(svgNS, button.type)
        el.id = `button-${button.id}`

    Object
      .keys(button.params)
      .map((param, i) => {
        el.setAttributeNS(null, param, button.params[param])
      })

    svg.append(el)
  })

  return svg
}

function onPageLoad() {
  if (!svg)
  {
    svg = createGamepadDisplay()
    svg.classList.add('hidden')
    svg.id = 'svg'
    document.getElementById('application').append(svg)
  }

  window.addEventListener('gamepadconnected', onGamepadConnected)
  window.addEventListener('gamepaddisconnected', onGamepadDisconnected)

  gamepadLoop()
}

const GamepadButtons = [
  { id: 0 , name: 'A', type: 'circle', params: {cx: 83, cy: 54, r: 5} },
  { id: 1 , name: 'B', type: 'circle', params: {cx: 92, cy: 45, r: 5} },
  { id: 2 , name: 'X', type: 'circle', params: {cx: 73, cy: 45, r: 5} },
  { id: 3 , name: 'Y', type: 'circle', params: {cx: 83, cy: 36, r: 5} },

  { id: 4, name: 'LB', type: 'circle', params: { cx: 27, cy: 24, r: 5 } },
  { id: 5, name: 'RB', type: 'circle', params: { cx: 71, cy: 24, r: 5 } },
  { id: 6, name: 'LT', type: 'circle', params: { cx: 16, cy: 19, r: 5 } },
  { id: 7, name: 'RT', type: 'circle', params: { cx: 82, cy: 19, r: 5 } },

  { id: 8 , name: 'SL', type: 'circle', params: {cx: 37.5, cy: 36.5, r: 2.5} },
  { id: 9 , name: 'ST', type: 'circle', params: {cx: 62.5, cy: 36.5, r: 2.5} },
  { id: 10, name: 'LS', type: 'circle', params: {cx: 15, cy: 45, r: 10, stroke: 'black', 'stroke-width': 3, fill: 'transparent'} },
  { id: 11, name: 'RS', type: 'circle', params: {cx: 63, cy: 67, r: 10, stroke: 'black', 'stroke-width': 3, fill: 'transparent'} },

  { id: 12, name: 'UP' , type: 'path', params: { d: 'M 28 51 L 38 51 L 38 63 L 33 68 L 28 63 L 28 51 Z'}},
  { id: 13, name: 'DWN', type: 'path', params: { d: 'M 28 85 L 38 85 L 38 73 L 33 68 L 28 73 L 28 85 Z'}},
  { id: 14, name: 'LFT', type: 'path', params: { d: 'M 16 63 L 16 73 L 28 73 L 33 68 L 28 63 L 16 63 Z'}},
  { id: 15, name: 'RGT', type: 'path', params: { d: 'M 50 63 L 50 73 L 38 73 L 33 68 L 38 63 L 50 63 Z'}},

  { id: 16 , name: 'XBOX', type: 'circle', params: {cx: 50, cy: 28, r: 4} },
  { id: 17 , name: 'X1', type: 'circle', params: {cx: 15, cy: 45, r: 4} },
  { id: 18 , name: 'X2', type: 'circle', params: {cx: 63, cy: 67, r: 4} },
  { id: 19 , name: 'LT_border', type: 'circle', params: {cx: 16, cy: 19, r: 5.5, fill: 'transparent', stroke: 'rgba(0,0,0, 1)', 'stroke-width': 1} },
  { id: 20 , name: 'RT_border', type: 'circle', params: {cx: 82, cy: 19, r: 5.5, fill: 'transparent', stroke: 'rgba(0,0,0, 1)', 'stroke-width': 1} },
]

onPageLoad()
