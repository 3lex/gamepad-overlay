import * as http from 'http'
import path from 'path'
import os from 'os'

import express from 'express'
import socketio from 'socket.io'

const OSIF = os.networkInterfaces()
const PORT = 1337

var IP: string

Object.keys(OSIF).forEach(ifname => {
  OSIF[ifname].forEach(iface => {
    if ('IPv4' !== iface.family || iface.internal !== false) return
    IP = iface.address
  })
})

const Gamepad = {
  id: null,
  axes: [],
  buttons: [],

  reset() {
    this.id = null
    this.axes = []
    this.buttons = []
  }
}

const app = express()

      app.set('trust proxy', true)

      app.use('/static', express.static(__dirname + '/public/assets'))
      app.use(express.json())
      app.use(express.urlencoded({ extended: true }))

      app.get('/', (req, res, next) => {
        console.log(``)
        console.log(`# Http GET Request`)
        console.log(`  Host      :`, req.headers['host'])
        console.log(`  User Agent:`, req.headers['user-agent'])
        console.log(``)

        res.sendFile(path.resolve(__dirname, 'public/index.html'))
      })

      app.get('/client.js', (req, res, next) => {
        console.log(`  :> File Request:`, 'client.js')
        console.log(``)

        res.sendFile(path.resolve(__dirname, 'public/client.js'))
      })

const server = new http.Server(app)
      server.listen(PORT, () => {
        console.log(`  ___   __   _  _  ____  ____   __   ____        __   _  _  ____  ____  __     __   _  _ `)
        console.log(` / __) / _\\ ( \\/ )(  __)(  _ \\ / _\\ (    \\ ___  /  \\ / )( \\(  __)(  _ \\(  )   / _\\ ( \\/ )`)
        console.log(`( (_ \\/    \\/ \\/ \\ ) _)  ) __//    \\ ) D ((___)(  O )\\ \\/ / ) _)  )   // (_/\\/    \\ )  / `)
        console.log(` \\___/\\_/\\_/\\_)(_/(____)(__)  \\_/\\_/(____/      \\__/  \\__/ (____)(__\\_)\\____/\\_/\\_/(__/  `)
        console.log(``)
        console.log('# Gamepad-Server online')
        console.log(`  IP-Address : ${IP}`)
        console.log(`  PORT       : ${PORT}`)
        console.log(``)
        console.log(`  Connect to url`)
        console.log(`  'http://${IP}:${PORT}'`)
        console.log(``)
        console.log(`  open the url in chrome/firefox on the machine you have your controller plugged in.`)
        console.log(`  make sure your gamepad is the first device in the gamepad-list and press any controller button to activate.`)
        console.log(`  open the same url in your streaming software like OBS/Streamlabs with the browser-plugin.`)
        console.log(``)
      })


const io = socketio(server)
      io.on('connection', onSocketConnected)

function onSocketConnected(socket: any): void {
  console.log(`# Socket connected.`)
  console.log(`  socket.id: `, socket.id)
  console.log(``)

  socket.emit('gamepad', Gamepad)
  socket.on('disconnect', onSocketDisconnect)
  socket.on('gamepad-connected', onGamepadConnected)
  socket.on('gamepad-update', onGamepadUpdate)
}

function onSocketDisconnect(): void {
  console.log(`# Socket disconnected.`)
  console.log(``)
}

function onGamepadConnected(state: boolean): void {
  console.log(`# Gamepad`, state ? 'detected' : 'disconnected')
  if (!state) Gamepad.reset()

  io.emit('gamepad', {
    id      : Gamepad.id,
    axes    : Gamepad.axes,
    buttons : Gamepad.buttons})
}

function onGamepadUpdate(gp: any): void {
  Gamepad.id = gp.id
  Gamepad.axes = gp.axes
  Gamepad.buttons = gp.buttons

  io.emit('gamepad', {
    id      : Gamepad.id,
    axes    : Gamepad.axes,
    buttons : Gamepad.buttons})
}
