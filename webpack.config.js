const path = require('path')

const Html = require('html-webpack-plugin')
const Copy = require('copy-webpack-plugin')
const Clean = require('clean-webpack-plugin')

module.exports = {
  mode: 'development',
  entry: {
    client: ['./src/pub/index.js']
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dist/public')
  },
  resolve: {
    alias: {
      Styles: path.resolve(__dirname, 'src/pub/sass')
    }
  },
  module: {
    rules: [

      {
        test: /\.jsx?$/,
        exclude: file => {
          /node_modules/.test(file) && !/\.vue\.js/.test(file)
        },
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
            compact: false
          }
        }
      },

      {
        test: /\.sass$/,
        use : [
          {
            loader: 'style-loader'
          },
          {
            loader: 'css-loader'
          },
          {
            loader: 'sass-loader',
            options: {
              indentedSyntax: true
            }
          },
        ]
      },

      {
        test: /\.css$/,
        use : [
          'style-loader',
          'css-loader'
        ]
      },

    ]
  },

  plugins: [
    // new Clean('./dist/public'),
    // new Copy([
    //   { from: './assets', to: 'assets'}
    // ]),
    new Html({
      title: 'gamepad-overlay',
      template: path.resolve(__dirname, 'src/pub/index.html'),
      inject: 'body'
    })
  ]
}
