# Gamepad-Overlay  

Node.js application for displaying gamepad button presses over the local network.

## Installation

Download repository
```
git clone https://3lex@bitbucket.org/3lex/gamepad-overlay.git
```
Installation with NPM:
```
cd gamepad-overlay
npm install && npm run build

```
Installation with YARN:
```
cd gamepad-overlay
yarn install && yarn build

```

Start Gamepad-Server  
``npm run start``  
or  
``yarn start``

### Gamepad Button Ids

Browser [Gamepad-API](https://developer.mozilla.org/en-US/docs/Web/API/Gamepad_API "Gamepad-API https://developer.mozilla.org")

```
const GamepadButtonIDs = {  
  xbox: {  
    0: 'A',  
    1: 'B',  
    2: 'X',  
    3: 'Y',  
    4: 'LB',  
    5: 'RB',  
    6: 'LT',  
    7: 'RT',  
    8: 'LS',  
    9: 'RS',  
    10:'SL',  
    11:'ST',  
    12:'UP',  
    13:'DWN',  
    14:'LFT',  
    15:'RGT',  
    16:'BOX'  
  }  
}  
```
